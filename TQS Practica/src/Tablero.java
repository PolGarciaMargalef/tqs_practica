
//import java.util.Random;

public class Tablero {

	
    
    public int filas; //Numero de filas
    public int columnas; //Numero de columnas
    public int bombas; //Numero de bombas totales en el juego
    public int[][] tablero;
    
    
    //Valores para asignar a Tablero sin ser personalizado por el jugador.
    final static int DIM_X = 8;
    final static int DIM_Y = 8;
    final static int BOMBS = 10;
    
    final static int MAX_DIM_X = 16; //MAXIMAS FILAS PERMITIDAS
    final static int MAX_DIM_Y = 16; //MAXIMAS COLUMNAS PERMITIDAS
    
    //Constructor Tablero por Defecto.
    public Tablero() {
    	
    	
    	
    	
    	filas= DIM_X;
    	columnas= DIM_Y;
    	bombas=BOMBS;
    	
    	
    }
    
    //Constructor Tablero Personalizado.
    
    public Tablero(int nfilas,int ncolumnas/*, int nBombas*/){
        
    	
    	if( nfilas < 0 )
    	{
    		nfilas = 0;
    	}
    	else if(nfilas > MAX_DIM_X)
    	{
    		nfilas=MAX_DIM_X;
    	}
    	
    	filas = nfilas;
    	
    	if( ncolumnas < 0)
    	{
    		ncolumnas = 0;
    	}
    	
    	else if (ncolumnas > MAX_DIM_Y)
    	{
    		ncolumnas = MAX_DIM_Y;
    	}
    	
    	columnas = ncolumnas;
    	
    	if(filas==0)
    		columnas=0;
    	if(columnas==0)
    		filas=0; 
    	
    	
        tablero= new int[filas][columnas];
        
        
        
        //bombas=nBombas;
        //inicializarTablero(nfilas,ncolumnas,nBombas);
	
    }
    
    //FALTA CONTROLAR QUE NO REPITA NUMERO ALEATORIO.
    public void inicializarTablero() {
    	
    	int contador = 0; 
    	int valorMaxAleatorio = 1;
    	
    	/*
    	Random valorRandom = new Random(); //valor aleatorio para acceder a la matriz 
    	
    	int posicionRandomFila = valorRandom.nextInt(tablero.length);
    	int posicionRandomColumna = valorRandom.nextInt(tablero.length);
    	*/

    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{

    			tablero[i][j]=1; //Todas las posiciones igual 1 ( = sin bomba )
    			//tablero[i][j]= (int) (Math.random() * valorMaxAleatorio); //Genera valor random del 0-1 , y asigna a casilla.
    		}
    	}
    	
    	
    	 // valor random entre 0 y el tama�o de la matriz. Si nfilas = 10, entonces nos dar� un valor entre 0 y 9

    	while(contador<bombas)
    	{
    		 int posicionRandomFila = (int) (Math.random() * filas);
        	 int posicionRandomColumna = (int) (Math.random() * columnas);
    		 tablero[posicionRandomFila][posicionRandomColumna] = 0; //Asignamos a la posicion aleatoria el valor de 0 ( = contiene bomba)
    		 contador++;	
    		 
    	}
    	
    	
    	System.out.println(contador);
    	
    }
    
    
    public void mostrarTablero() {
    	
    	
    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{
    			System.out.println(tablero[i][j]);
    			
    		}
    	}
    	
    	
    	
    	
    	
    }
    
    public void setBombas(int nbombas) {
    	bombas=nbombas;
    }
    
    public int getFilas(){
    	return filas;
    }
    
    public int getColumnas(){
    	return columnas;
    }
    
    public int getBombas(){
    	return bombas;
    }
    
    
    
    
    
    
}
