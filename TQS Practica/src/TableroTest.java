import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class TableroTest {

	 //Valores para asignar a Tablero sin ser personalizado por el jugador.
    final static int DIM_X = 8;
    final static int DIM_Y = 8;
    final static int BOMBS = 10;
    
    final static int MAX_DIM_X = 16; //MAXIMAS FILAS PERMITIDAS
    final static int MAX_DIM_Y = 16; //MAXIMAS COLUMNAS PERMITIDAS
    Tablero tablero1;
    Tablero tablero2;
	
	@BeforeEach
	void setUp() throws Exception {
		
	}

	
	
	
	@Test
	
	// Comprueba las inicializaciones del constructor por defecto. Es decir, con dimensiones por defecto (DIM_X,DIM_Y)
	// y n�Bombas.
	public void testConstructor() {
		
		int[][] matrizDefecto = new int[DIM_X][DIM_Y];
		
		tablero1 = new Tablero();
		
		int[][] matrizTablero = new int[tablero1.getFilas()][tablero1.getColumnas()];
		
		assertArrayEquals(matrizDefecto,matrizTablero);
		
		
		
	}	
	
	@Test
	
	//Comprovamos que funcionan los constructores y comparamos diferentes instancias de Tablero.
	public void testTableroEquals() {
		
		tablero1 = new Tablero(5,5);
		tablero2= new Tablero(8,16);
		
		Tablero tablerotest = new Tablero(10,10);
		
		assertEquals(tablero1,tablero1);
		assertEquals(tablero2,tablero2);
		
		assertTrue(!tablero1.equals(null));
		assertTrue(!tablero2.equals(null));
		
		assertFalse(tablero1.equals(tablero2));
		assertFalse(tablero2.equals(tablerotest));
		assertFalse(tablero1.equals(tablerotest));
		
	}
	
	
	@Test
	void testConstructorPersonalizado() 
	{
		int casosTotales 			= 9;

		int[][] testingmatrix 	=
		{ 
			{ DIM_X, DIM_Y },						// Inputs v�lidos.
			{ MAX_DIM_X + 1, MAX_DIM_Y + 1},
			{ MAX_DIM_X, MAX_DIM_Y},
			{ -1, -1 },								// Inputs no v�lidos, n�meros negativos o valores = 0. 
			{ -1, DIM_Y},							
			{ DIM_X, -1 },							
			{ 0, 0 },								
			{ 0, DIM_Y },							
			{ DIM_X, 0 }							
		};
		
		int[][] exptd	=
		{
		    { DIM_X, DIM_Y },						// Valor esperado ser� el de los inputs ya que estan dentro de las fronteras.
		    { MAX_DIM_X, MAX_DIM_Y },				// Valor esperado ser�n los MAX, ya que cualquier numero que supere la frontera maxima permitida ser� = constante MAX.
			{ MAX_DIM_X,  MAX_DIM_Y },
		
			{ 0, 0 },								// El resultado esperado tiene que ser {0,0} ya que los inputs en caso de ser nfilas = 0
			{ 0, 0 },								// columnas tambi�n ser� = 0. Lo mismo para columnas y numeros negativos.
			{ 0, 0 },								
			{ 0, 0 },								
			{ 0, 0 },								
			{ 0, 0 }								
		};
		
		// El array res_d contiene las filas y columnas que se obtienen mediante las funciones get y que recogen los valores que tenemos en nuestra matriz de testeo testingmatrix.
		for ( int i = 0 ; i < casosTotales ; i++ )
		{
			tablero1 		= new Tablero(testingmatrix[i][0], testingmatrix[i][1]);
			int[] res_d = { tablero1.getFilas(), tablero1.getColumnas() };

			
			assertArrayEquals(exptd[i], res_d); //Comparamos el valor esperado con el resultado obtenido de los getters de filas y columnas.
			
		}
	}
	
	
	
	@Test
	
	public void testinicializarTablero() {
		
		tablero1 = new Tablero(5,5);
		
		int i,j;
		int[][]res_d = new int[5][5];
		
		int[][] exptd = {
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1}
		};
		
		for(i=0;i<tablero1.getFilas();i++)
    	{
    		for (j=0 ;j<tablero1.getColumnas();j++)
    		{

    			res_d[i][j]=1; //Todas las posiciones igual 1 ( = sin bomba )
    			

    		}
    		
    		
    		assertTrue(tablero1.getColumnas()==j); //Comprobamos que no exceda el m�ximo de columnas ( 5 )
    	}
		assertTrue(tablero1.getFilas()==i); //Comprobamos que no exceda el m�ximo de filas ( 5 )
		
		//Comprobamos que toda la matriz res_d se haya inicializado en todas sus posiciones (5x5) a 1.
		assertArrayEquals(exptd,res_d);
	}
	
	
	
	@Test
	public void testinicializarTableroBombPositions() { //Test de la colocaci�n de bombas en posiciones aleatorias del tablero.
		
		tablero1 = new Tablero(5,5);  //Seteamos n� Filas y de Columnas
		tablero1.setBombas(6);  //Seteamos las bombas con las que jugaremos
		int contador = 0;
		
		int[][] exptd = {
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1}
		};
		
		int[][]res_d = {
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1},
				{1,1,1,1,1}
			};
		
		
		//Nos aseguramos que el numero aleatorio generado de tipo enteros entre los valores 0 y los valores de
		//filas y columnas proporcionados al instanciar tablero1, siempre sea inferior o igual al m�ximo establecido de filas y columnas.
		
		for( int i = 0 ; i < tablero1.getBombas() ; i++) {
			
			int posicionRandomFila = (int) (Math.random() * tablero1.getFilas());
       	 	int posicionRandomColumna = (int) (Math.random() * tablero1.getColumnas());
			
       	 	exptd[posicionRandomFila][posicionRandomColumna]=0;
       	 	res_d[posicionRandomFila][posicionRandomColumna]=0;
       	 	
       	 	assertTrue(posicionRandomFila<=tablero1.getFilas());
       	 	assertTrue(posicionRandomColumna<=tablero1.getColumnas());
       	 	contador++;
       	 	
       	 	
		}
		
		
		
		assertTrue(contador==tablero1.getBombas());  //Assert para asegurarnos de que el bucle for se realice hasta llegar al valor m�ximo de bombas.
		
		//Comprovamos que una vez generados los valores aleatorios , las bombas son asignadas a estas posiciones
		//y ambas matrices las tienen colocadas correctamente y en la misma posicion.
		assertArrayEquals(exptd,res_d);
		
		
	}
	
	
	
	

}
